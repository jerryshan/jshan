# me



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:cb2210d974c0e3d27356b0b55f6a796a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:cb2210d974c0e3d27356b0b55f6a796a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:cb2210d974c0e3d27356b0b55f6a796a?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jerryshan/me.git
git branch -M main
git push -uf origin main
```




***

# 🏆 Achievements
### Analytics for Course Engagement (ACE) project
- Winner of the 2021 CAUDIT Award in the Improving Student Success category
- Finalist of the 2021 NZ Open-Source Award in the Education, Social Services and Youth category (results pending)


# 🔧 Technologies & Tools
## Operating Systems
![](https://img.shields.io/badge/OS-Linux-informational?style=flat&logo=linux&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/OS-Windows-informational?style=flat&logo=windows&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/OS-MacOS-informational?style=flat&logo=apple&logoColor=white&color=2bbc8a)

## Programming languages
![](https://img.shields.io/badge/Code-PHP-informational?style=flat&logo=php&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-CSharp-informational?style=flat&logo=csharp&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-C-informational?style=flat&logo=c&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-VisualBasic-informational?style=flat&logo=visualbasic&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-C++-informational?style=flat&logo=cplusplus&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-java-informational?style=flat&logo=java&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-VB.net-informational?style=flat&logo=vbdotnet&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Shell-Bash-informational?style=flat&logo=shell&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-JADE-informational?style=flat&logo=jade&logoColor=white&color=2bbc8a)

## Process Automation
![](https://img.shields.io/badge/Auto-TestStudio-informational?style=flat&logo=automate&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Auto-Selenium-informational?style=flat&logo=selenium&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Auto-AutoIT-informational?style=flat&logo=autoit&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Auto-TestComplete-informational?style=flat&logo=testcomplete&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Auto-SoapUI-informational?style=flat&logo=soapui&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Auto-JMeter-informational?style=flat&logo=jmeter&logoColor=white&color=2bbc8a)

## Database
![](https://img.shields.io/badge/DB-PostgreSQL-informational?style=flat&logo=postgresql&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/DB-MySQL-informational?style=flat&logo=mysql&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/DB-SQLite-informational?style=flat&logo=sqlite&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/DB-MSSQL-informational?style=flat&logo=microsoft&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/DB-MSAccess-informational?style=flat&logo=access&logoColor=white&color=2bbc8a)


## Web Development
![](https://img.shields.io/badge/Web-HTML-informational?style=flat&logo=html&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-XML-informational?style=flat&logo=xml&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-CSS-informational?style=flat&logo=css&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-CSS3-informational?style=flat&logo=css3&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-HTML5-informational?style=flat&logo=html5&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-Ajax-informational?style=flat&logo=ajax&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-JavaScript-informational?style=flat&logo=javascript&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-JQuery-informational?style=flat&logo=jquery&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/-Git-informational?style=flat&logo=git&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Web-Botstrap-informational?style=flat&logo=bootstrap&logoColor=white&color=2bbc8a)


![](https://img.shields.io/badge/OpenSoure-Moodle-informational?style=flat&logo=open&logoColor=white&color=2bbc8a)

## Tools
![](https://img.shields.io/badge/Tools-Docker-informational?style=flat&logo=docker&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Kubernetes-informational?style=flat&logo=kubernetes&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Ansible-informational?style=flat&logo=ansible&logoColor=white&color=2bbc8a)

